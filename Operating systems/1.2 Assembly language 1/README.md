1.

Write code that swaps the values ​​in the RSI and RDX registers. That is, if before the execution of your code, the number 1 is stored in the RSI register, and the number 2 is stored in the RDX register, then after the execution of the code, the number 2 should be stored in the RSI register, and the number 1 in the RDX register.

You are allowed to use the following general purpose registers: RAX, RBX, RCX, RDX, RBP, RDI, RSI, R8 - R15.

The job is not supposed to use a stack, even if you know what it is.

2.

Add two numbers in the RSI and RDX registers, the result should be in the RSI register.

You are allowed to use the following general purpose registers: RAX, RBX, RCX, RDX, RBP, RDI, RSI, R8 - R15.

The job is not supposed to use a stack, even if you know what it is.

3.

In the RSI register, you are given an integer - degrees Fahrenheit (TF). Write a code that will get the corresponding Celsius value in the RSI register (TVS)).
Formula to convert from Fahrenheit to Celsius:

TVS = 5×(TF - 32)/9

IMPORTANT﻿ div and mul instructions cannot be simply a number (aka immediate) as an operand, i.e. you have to load the divisor/multiplier into a register and use the register.

Since we did not consider operations with non-integer numbers, we need to discard the non-integer part of the result and return only the integer part. In addition, the input number and the response are guaranteed to be unsigned numbers (more formally, it is guaranteed that 32≤TF≤220).

You are allowed to use the following general purpose registers: RAX, RBX, RCX, RDX, RBP, RDI, RSI, R8 - R15.

The job is not supposed to use a stack, even if you know what it is.

4. 
In the RSI and RDX registers, you are given two numbers, your task is to swap them, as in one of the previous tasks. But the condition is added that all other general purpose registers must remain unchanged. That is, if you use some kind of general-purpose register other than RSI and RDX, then you must save and then restore the saved value of the register (well, or not use these registers at all).

You are allowed to use the following general purpose registers: RAX, RBX, RCX, RDX, RBP, RDI, RSI, R8 - R15.

The job assumes the use of a stack, but you must restore the stack to its original state.
